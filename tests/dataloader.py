# -*- coding: utf-8 -*-
# internal libraries

# external libraries
import numpy as np
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

# custom libraries

np.random.seed(0)

class TestDataset(Dataset):
    def __init__(self, n_samples=10, feature_size=10):
        super(TestDataset, self).__init__()
        self.x, self.y = self.create_samples(n_samples, feature_size)
    
    def create_samples(self, n_samples, feature_size):
        x = np.array([np.linspace(0, 1, num=n_samples)]*feature_size).transpose()
        y = x[:, 0]**2 + 4 + np.random.randn(n_samples)
        return x, y
    
    def __getitem__(self, idx):
        return torch.Tensor(self.x[idx]).float(), torch.Tensor([self.y[idx]]).float()
    
    def __len__(self):
        return self.x.shape[0]
    