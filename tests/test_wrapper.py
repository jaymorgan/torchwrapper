# -*- coding: utf-8 -*-

# internal libraries
import unittest
import random
import sys
sys.path.append('../')

# external libraries
import numpy as np
import torch
from torch.utils.data import DataLoader

# custom libraries
from torchwrapper import Wrapper
from tests.basicmodel import BasicModel
from tests import dataloader

random.seed(0)
torch.manual_seed(0)
np.random.seed(0)


class TestWrapper(unittest.TestCase):
    def setUp(self):
        self.model = BasicModel()
        self.optimizer = torch.optim.Adam(self.model.parameters())
        self.criterion = torch.nn.MSELoss()
        self.wrapper = Wrapper(self.model)
        self.dataloader = DataLoader(dataloader.TestDataset())
        
    def test_init(self):
        w = Wrapper()
        self.assertEqual(w.model, None)
        w = Wrapper(self.model)
        self.assertEqual(w.model, self.model)
        
    def test__transfer_tensor(self):
        x = torch.Tensor([[0, 1], [1, 0]])
        new_x = self.wrapper._transfer_tensor(x)
        self.assertEqual(type(new_x), np.ndarray)
        self.assertNotEqual(type(x), np.ndarray)
        self.assertNotEqual(type(x), type(new_x))

        # check that the data is still intact
        self.assertEqual(new_x.shape, (2, 2))
        self.assertEqual(new_x[0, 1], 1)
        self.assertEqual(new_x[1, 1], 0)
        
    def test_set_model(self):
        w = Wrapper()
        self.assertEqual(w.model, None)
        w.set_model(self.model)
        self.assertNotEqual(w.model, None)
        self.assertEqual(w.model, self.model)
        
    def test__run_epoch(self):
        y_hats = self.wrapper._run_epoch(self.dataloader, verbose=False)
        self.assertEqual(y_hats.shape[0], len(self.dataloader))
        self.assertEqual(y_hats[0], 0.12911409)
        
    def test_fit(self):
        y_hats = self.wrapper.fit(self.dataloader, self.optimizer, self.criterion, epochs=1, verbose=0)
        self.assertEqual(y_hats.shape[0], len(self.dataloader))
        self.assertEqual(y_hats[0], 0.17963207)
        y_hats = self.wrapper.fit(self.dataloader, self.optimizer, self.criterion, epochs=10, verbose=0)
        self.assertEqual(y_hats.shape[0], len(self.dataloader))
        self.assertEqual(y_hats[0], 0.4816377)
        self.wrapper.fit(self.dataloader, self.optimizer, self.criterion, verbose=0)

    def test_predict(self):
        preds = self.wrapper.predict(self.dataloader)
        self.assertEqual(preds[0], -0.29844114)
        self.assertEqual(preds.shape[0], len(self.dataloader))
        self.wrapper.fit(self.dataloader, self.optimizer, self.criterion, epochs=2, verbose=0)
        preds = self.wrapper.predict(self.dataloader)
        self.assertEqual(preds[0], -0.26104245)


if __name__ == '__main__':
    unittest.main()